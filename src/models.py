#!/usr/bin/env python3
# coding=utf-8

from sqlalchemy import Column, Integer, String, Text
from src.database_settings import init_db, Base


class Log(Base):
	__tablename__ = 'logs'
	id = Column(Integer, primary_key=True)
	user_ip = Column(String(100))
	title = Column(Text)
	text = Column(Text)

	def __init__(self, title=None, text=None, user_ip=None):
		self.title = title
		self.text = text
		self.user_ip = user_ip

	def __repr__(self):
		return self.title
