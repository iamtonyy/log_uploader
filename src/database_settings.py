#!/usr/bin/env python3
# coding=utf-8

import os
import sqlite3

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from src import config


engine = create_engine(config.DATABASE_URL, convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
	# Create database if not exist
	if not os.path.exists(config.DATABASE_PATH):
		connection = sqlite3.connect(config.DATABASE_PATH)
		connection.close()

	Base.metadata.create_all(bind=engine)
