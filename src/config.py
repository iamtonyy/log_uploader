#!/usr/bin/env python3
# coding=utf-8

import os


DATABASE_PATH = os.path.abspath(os.environ.get('DATABASE_URL', 'database/app_database.db'))
DATABASE_URL = 'sqlite:////' + DATABASE_PATH
