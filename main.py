#!/usr/bin/env python3
# coding=utf-8

import os
import src.config as config

from flask import Flask, request, redirect, url_for, render_template, jsonify
from werkzeug.utils import secure_filename
from src.database_settings import db_session, init_db
from src.models import Log


app = Flask(__name__)


@app.route("/", methods=['GET'])
def index():
	return render_template('index.html')


@app.route("/all_logs", methods=['GET'])
def get_logs():
	remote_address = str(request.remote_addr)
	logs = Log.query.filter_by(user_ip=remote_address)

	response = {}
	for log in logs:
		response[log.id] = str(log.title)

	response = jsonify(response)
	return response, 200


@app.route("/log/<int:log_id>", methods=['GET'])
def get_log(log_id):
	remote_address = str(request.remote_addr)
	log = Log.query.filter_by(id=log_id, user_ip=remote_address).first()

	if log is None:
		return redirect('/')

	return render_template('detail.html', title=str(log.title), text=str(log.text))


@app.route('/upload', methods=['POST'])
def upload_file():
	remote_address = str(request.remote_addr)
	# check if the post request has the file part
	if 'file' not in request.files:
		return redirect(request.url)

	file = request.files['file']
	# if user does not select file, browser also
	# submit a empty part without filename
	if file.filename == '':
		return redirect(request.url)

	if file:
		filename = secure_filename(file.filename)
		text = file.read()
		log = Log(title=filename, text=text, user_ip=remote_address)
		db_session.add(log)
		db_session.commit()

		return redirect(url_for('get_log', log_id=log.id))


@app.teardown_appcontext
def shutdown_session(exception=None):
	"""Flask will automatically remove database sessions
	at the end of the request or when the application shuts down.
	"""
	db_session.remove()



init_db()
app.run(host="0.0.0.0", port=os.environ.get('PORT', 5000))